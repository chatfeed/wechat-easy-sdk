package weixin.popular.bean.live;

public class ImportGoodsRequest {
    public Integer[] getIds() {
        return ids;
    }

    public void setIds(Integer[] ids) {
        this.ids = ids;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    private Integer[] ids;
    private Integer roomId;
}
