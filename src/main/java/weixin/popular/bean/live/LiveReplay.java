package weixin.popular.bean.live;

import com.alibaba.fastjson.annotation.JSONField;

public class LiveReplay {
    public String getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getMediaUrl() {
        return mediaUrl;
    }

    public void setMediaUrl(String mediaUrl) {
        this.mediaUrl = mediaUrl;
    }

    @JSONField(name = "expire_time")
    private String expireTime;
    @JSONField(name = "create_time")
    private String createTime;
    @JSONField(name = "media_url")
    private String mediaUrl;
}
