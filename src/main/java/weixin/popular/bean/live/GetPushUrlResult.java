package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

import java.util.List;

public class GetPushUrlResult extends BaseResult {
    public String getPushAddr() {
        return pushAddr;
    }

    public void setPushAddr(String pushAddr) {
        this.pushAddr = pushAddr;
    }

    String pushAddr;
}
