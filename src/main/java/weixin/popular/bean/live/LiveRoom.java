package weixin.popular.bean.live;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public class LiveRoom {
    public Integer getRoomid() {
        return roomid;
    }

    public void setRoomid(Integer roomid) {
        this.roomid = roomid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public Integer getStartTime() {
        return startTime;
    }

    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public Integer getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public Integer getLiveStatus() {
        return liveStatus;
    }

    public void setLiveStatus(Integer liveStatus) {
        this.liveStatus = liveStatus;
    }

    public String getAnchorName() {
        return anchorName;
    }

    public void setAnchorName(String anchorName) {
        this.anchorName = anchorName;
    }

    public String getAnchorWechat() {
        return anchorWechat;
    }

    public void setAnchorWechat(String anchorWechat) {
        this.anchorWechat = anchorWechat;
    }

    public String getSubAnchorWechat() {
        return subAnchorWechat;
    }

    public void setSubAnchorWechat(String subAnchorWechat) {
        this.subAnchorWechat = subAnchorWechat;
    }

    public String getCreaterWechat() {
        return createrWechat;
    }

    public void setCreaterWechat(String createrWechat) {
        this.createrWechat = createrWechat;
    }

    public String getCreaterOpenid() {
        return createrOpenid;
    }

    public void setCreaterOpenid(String createrOpenid) {
        this.createrOpenid = createrOpenid;
    }

    public String getShareImg() {
        return shareImg;
    }

    public void setShareImg(String shareImg) {
        this.shareImg = shareImg;
    }

    public String getFeedsImg() {
        return feedsImg;
    }

    public void setFeedsImg(String feedsImg) {
        this.feedsImg = feedsImg;
    }

    public Integer getIsFeedsPublic() {
        return isFeedsPublic;
    }

    public void setIsFeedsPublic(Integer isFeedsPublic) {
        this.isFeedsPublic = isFeedsPublic;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getCloseLike() {
        return closeLike;
    }

    public void setCloseLike(Integer closeLike) {
        this.closeLike = closeLike;
    }

    public Integer getCloseGoods() {
        return closeGoods;
    }

    public void setCloseGoods(Integer closeGoods) {
        this.closeGoods = closeGoods;
    }

    public Integer getCloseComment() {
        return closeComment;
    }

    public void setCloseComment(Integer closeComment) {
        this.closeComment = closeComment;
    }

    public Integer getCloseReplay() {
        return closeReplay;
    }

    public void setCloseReplay(Integer closeReplay) {
        this.closeReplay = closeReplay;
    }

    public Integer getCloseShare() {
        return closeShare;
    }

    public void setCloseShare(Integer closeShare) {
        this.closeShare = closeShare;
    }

    public Integer getCloseKf() {
        return closeKf;
    }

    public void setCloseKf(Integer closeKf) {
        this.closeKf = closeKf;
    }

    public List<Goods> getGoods() {
        return goods;
    }

    public void setGoods(List<Goods> goods) {
        this.goods = goods;
    }

    public Integer getLiveType() {
        return liveType;
    }

    public void setLiveType(Integer liveType) {
        this.liveType = liveType;
    }

    
    private Integer roomid;
    
    private String name;
    
    private String coverImg;
    
    private Integer startTime;
    
    private Integer endTime;
    
    private Integer liveStatus;
    
    private String anchorName;
    
    private String anchorWechat;
    
    private String subAnchorWechat;
    
    private String createrWechat;

    
    private String 	createrOpenid;
    
    private String shareImg;
    
    private String feedsImg;
    
    private Integer isFeedsPublic;
    
    private Integer type;
    
    private Integer closeLike;
    
    private Integer closeGoods;
    
    private Integer closeComment;
    
    private Integer closeReplay;
    
    private Integer closeShare;
    
    private Integer closeKf;
    
    private List<Goods> goods;
    
    private Integer liveType;

}
