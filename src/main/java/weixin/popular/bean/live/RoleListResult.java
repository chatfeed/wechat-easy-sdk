package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

import java.util.List;

public class RoleListResult extends BaseResult {
    private Integer total;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<RoleInfo> getList() {
        return list;
    }

    public void setList(List<RoleInfo> list) {
        this.list = list;
    }

    private List<RoleInfo> list;
}
