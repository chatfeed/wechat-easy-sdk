package weixin.popular.bean.live;

public class RoleInfo {
    public String getHeadingimg() {
        return headingimg;
    }

    public void setHeadingimg(String headingimg) {
        this.headingimg = headingimg;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public Integer[] getRoleList() {
        return roleList;
    }

    public void setRoleList(Integer[] roleList) {
        this.roleList = roleList;
    }

    public String getUpdateTimestamp() {
        return updateTimestamp;
    }

    public void setUpdateTimestamp(String updateTimestamp) {
        this.updateTimestamp = updateTimestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private String headingimg;
    private String nickname;
    private String openid;
    private Integer[] roleList;
    private String updateTimestamp;
    private String username;
}
