package weixin.popular.bean.live;

import com.alibaba.fastjson.annotation.JSONField;

public class User {

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @JSONField(name = "username")
    private String username;
    @JSONField(name = "nickname")
    private String nickname;
}
