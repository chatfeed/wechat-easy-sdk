package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

import java.util.List;

public class RoomListResult extends BaseResult {
    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<LiveRoom> getRoom_info() {
        return room_info;
    }

    public void setRoom_info(List<LiveRoom> room_info) {
        this.room_info = room_info;
    }

    Integer total;
    List<LiveRoom> room_info;
}
