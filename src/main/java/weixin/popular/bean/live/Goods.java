package weixin.popular.bean.live;

import com.alibaba.fastjson.annotation.JSONField;
public class Goods {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverImg() {
        return coverImg;
    }

    public void setCoverImg(String coverImg) {
        this.coverImg = coverImg;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getPrice2() {
        return price2;
    }

    public void setPrice2(Integer price2) {
        this.price2 = price2;
    }

    public Integer getPriceType() {
        return priceType;
    }

    public void setPriceType(Integer priceType) {
        this.priceType = priceType;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getThirdPartyAppid() {
        return thirdPartyAppid;
    }

    public void setThirdPartyAppid(String thirdPartyAppid) {
        this.thirdPartyAppid = thirdPartyAppid;
    }

    @JSONField(name = "name")
    private String name;
    @JSONField(name = "cover_img")
    private String coverImg;
    @JSONField(name = "url")
    private String url;
    @JSONField(name = "price")
    private Integer price;
    @JSONField(name = "price2")
    private Integer price2;
    @JSONField(name = "price_type")
    private Integer priceType;
    @JSONField(name = "goods_id")
    private Integer goodsId;
    @JSONField(name = "third_party_appid")
    private String thirdPartyAppid;





}
