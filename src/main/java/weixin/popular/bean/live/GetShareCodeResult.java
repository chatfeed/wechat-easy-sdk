package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

public class GetShareCodeResult extends BaseResult {

    public GetShareCodeResult(String errcode,String errmsg){
        super(errcode,errmsg);
    }
    public String getCdnUrl() {
        return cdnUrl;
    }

    public void setCdnUrl(String cdnUrl) {
        this.cdnUrl = cdnUrl;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    String cdnUrl;
    String pagePath;
    String posterUrl;
}
