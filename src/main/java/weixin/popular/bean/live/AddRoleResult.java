package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

public class AddRoleResult extends BaseResult {
    private String codeurl;

    public String getCodeurl() {
        return codeurl;
    }

    public void setCodeurl(String codeurl) {
        this.codeurl = codeurl;
    }
}
