package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

public class CreateRoomResult extends BaseResult {
    private Integer roomId;
    // 当主播微信号没有在 “小程序直播“ 小程序实名认证 返回该字段
    private String qrcodeUrl;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getQrcodeUrl() {
        return qrcodeUrl;
    }

    public void setQrcodeUrl(String qrcodeUrl) {
        this.qrcodeUrl = qrcodeUrl;
    }
}