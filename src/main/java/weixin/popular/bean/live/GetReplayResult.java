package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

import java.util.List;

public class GetReplayResult extends BaseResult {
    Integer total;
    List<LiveReplay> live_replay;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<LiveReplay> getLive_replay() {
        return live_replay;
    }

    public void setLive_replay(List<LiveReplay> live_replay) {
        this.live_replay = live_replay;
    }
}
