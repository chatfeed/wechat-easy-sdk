package weixin.popular.bean.live;

import weixin.popular.bean.BaseResult;

import java.util.List;

public class GetAssistantListResult extends BaseResult {
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    Integer count;
    Integer maxCount;
    List<Assistant> list;

    public List<Assistant> getList() {
        return list;
    }

    public void setList(List<Assistant> list) {
        this.list = list;
    }
}
