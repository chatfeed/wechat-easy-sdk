package weixin.popular.bean.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * 摇一摇 周边事件数据
 * 
 * @author LiYi
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="Agent")
public class Agent {
	
	@XmlElement(name="name")
	private String name;
	
	@XmlElement(name="phone")
	private String phone;
	
	@XmlElement(name="reach_time")
	private String reach_time;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getReach_time() {
		return reach_time;
	}

	public void setReach_time(String reach_time) {
		this.reach_time = reach_time;
	}
}
