package weixin.popular.bean.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * 摇一摇 周边事件数据
 * 
 * @author LiYi
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="ActionItem")
public class ActionItem {
	@XmlElement(name="ActionTime")
	private String ActionTime;
	
	@XmlElement(name="ActionType")
	private String ActionType;
	
	@XmlElement(name="ActionMsg")
	private String ActionMsg;

	public String getActionTime() {
		return ActionTime;
	}

	public void setActionTime(String actionTime) {
		ActionTime = actionTime;
	}

	public String getActionType() {
		return ActionType;
	}

	public void setActionType(String actionType) {
		ActionType = actionType;
	}

	public String getActionMsg() {
		return ActionMsg;
	}

	public void setActionMsg(String actionMsg) {
		ActionMsg = actionMsg;
	}

}
