package weixin.popular.api;

import com.alibaba.fastjson.JSON;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weixin.popular.bean.BaseResult;
import weixin.popular.bean.live.*;
import weixin.popular.client.LocalHttpClient;
import weixin.popular.util.JsonUtil;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;

public class WxaLiveAPI extends BaseAPI {
    private static Logger logger = LoggerFactory.getLogger(WxaLiveAPI.class);

    /**
     * 创建直播间
     * @param access_token access_token
     * @param liveRoom liveRoom
     * @return weixin.popular.bean.live.CreateRoomResult
     * @author chatfeed
     */
    public static CreateRoomResult createRoom(String access_token, LiveRoom liveRoom){
        String json = JSON.toJSONString(liveRoom);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/create")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,CreateRoomResult.class);
    }

    /**
     * 直播间列表
     * @param access_token access_token
     * @param start start
     * @param limit limit
     * @return weixin.popular.bean.live.RoomListResult
     * @author chatfeed
     */
    public static RoomListResult roomList(String access_token, Integer start, Integer limit){
        HashMap<String, Integer> stringIntegerHashMap = new HashMap<String,Integer>();
        stringIntegerHashMap.put("start",start);
        stringIntegerHashMap.put("limit",limit);
        String json = JsonUtil.toJSONString(stringIntegerHashMap);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxa/business/getliveinfo")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,RoomListResult.class);

    }
    /**
     * 获取直播间回放
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param start start
     * @param limit limit
     * @return weixin.popular.bean.live.GetReplayRequest
     * @author chatfeed
     */
    public static GetReplayRequest getReplay(String access_token,Integer wxRoomId,Integer start,Integer limit){
        GetReplayRequest request = new GetReplayRequest();
        request.setAction("get_replay");
        request.setRoom_id(wxRoomId);
        request.setStart(start);
        request.setLimit(limit);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxa/business/getliveinfo")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,GetReplayRequest.class);
    }

    /**
     * 导入商品
     * @param access_token access_token
     * @param ids ids
     * @param roomId roomId
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult importGoods(String access_token,Integer[] ids,Integer roomId){
        ImportGoodsRequest request = new ImportGoodsRequest();
        request.setIds(ids);
        request.setRoomId(roomId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/addgoods")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest, BaseResult.class);
    }

    /**
     * 删除直播间
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult deleteRoom(String access_token,Integer wxRoomId){
        HashMap<String, Integer> request = new HashMap<String,Integer>();
        request.put("id",wxRoomId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/deleteroom")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest, BaseResult.class);

    }

    /**
     * 编辑直播间
     * @param access_token access_token
     * @param liveRoom liveRoom
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult editRoom(String access_token, LiveRoom liveRoom){
        String json = JsonUtil.toJSONString(liveRoom);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/editroom")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     * 获取直播间推流地址
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @return weixin.popular.bean.live.GetPushUrlResult
     * @author chatfeed
     */
    public static GetPushUrlResult getPushUrl(String access_token,Integer wxRoomId){
        HashMap<String, Integer> request = new HashMap<String,Integer>();
        request.put("roomId",wxRoomId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/getpushurl")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,GetPushUrlResult.class);
    }

    public static GetShareCodeResult getShareCodeResult(String access_token,Integer wxRoomId,String param){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        try {
            request.put("params", URLEncoder.encode(param,"utf-8"));
        } catch (UnsupportedEncodingException e) {
            return new GetShareCodeResult("-1",e.getMessage());
        }
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/getsharedcode")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,GetShareCodeResult.class);

    }

    /**
     *  添加管理直播间小助手
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param users users
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult addAssistant(String access_token,Integer wxRoomId,User[] users){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("users", users);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/addassistant")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     * 修改管理直播间小助手
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param username username
     * @param nickname nickname
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult modifyAssistant(String access_token,Integer wxRoomId,String username,String nickname){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("username", username);
        request.put("nickname", nickname);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/modifyassistant")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);

    }
    /**
     * 删除管理直播间小助手
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param username username
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult removeAssistant(String access_token,Integer wxRoomId,String username){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("username", username);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/removeassistant")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);

    }
    /**
     * 查询管理直播间小助手
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @return weixin.popular.bean.live.GetAssistantListResult
     * @author chatfeed
     */
    public static GetAssistantListResult getAssistantList(String access_token,Integer wxRoomId){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/getassistantlist")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,GetAssistantListResult.class);

    }

    /**
     *  添加主播副号
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param username username
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult addSubAnchor(String access_token,Integer wxRoomId,String username){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("username", username);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/addsubanchor")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     *  修改主播副号
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param username username
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult modifySubAnchor(String access_token,Integer wxRoomId,String username){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("username", username);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/modifysubanchor")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);

    }

    /**
     * 删除主播副号
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult deleteSubAnchor(String access_token,Integer wxRoomId){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/deletesubanchor")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);

    }

    /**
     * 开启/关闭直播间官方收录
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param isFeedsPublic isFeedsPublic
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult updateFeedPublic(String access_token,Integer wxRoomId,Integer isFeedsPublic){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("isFeedsPublic",isFeedsPublic);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/updatefeedpublic")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     * 开启/关闭回放功能
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param closeReplay closeReplay
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult updateReplay(String access_token,Integer wxRoomId,Integer closeReplay){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("closeReplay",closeReplay);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/updatereplay")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     *  开启/关闭客服功能
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param closeKf closeKf
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult updateKf(String access_token,Integer wxRoomId,Integer closeKf){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("closeKf",closeKf);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/updatekf")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     * 开启/关闭直播间全局禁言
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param banComment banComment
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult updateComment(String access_token,Integer wxRoomId,Integer banComment){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("banComment",banComment);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/room/updatecomment")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     *  上下架商品
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param goodsId goodsId
     * @param onSale onSale
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult onSale(String access_token,Integer wxRoomId,Integer goodsId,Integer onSale){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("goodsId",goodsId);
        request.put("onSale",onSale);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/goods/onsale")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     *  删除商品
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param goodsId goodsId
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult deleteInRoom(String access_token,Integer wxRoomId,Integer goodsId){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("goodsId",goodsId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/goods/deleteInRoom")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    /**
     *  推送商品
     * @param access_token access_token
     * @param wxRoomId wxRoomId
     * @param goodsId goodsId
     * @return weixin.popular.bean.BaseResult
     * @author chatfeed
     */
    public static BaseResult push(String access_token,Integer wxRoomId,Integer goodsId){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("goodsId",goodsId);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/goods/push")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);
    }

    public static BaseResult goodsSort(String access_token, Integer wxRoomId, List<GoodsRequest> goods){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("roomId",wxRoomId);
        request.put("goods",goods);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/goods/sort")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,BaseResult.class);

    }

    public static AddGoodsResult addGoods(String access_token,Goods goods){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("goodsInfo",goods);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/goods/add")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,AddGoodsResult.class);

    }


    public static AddRoleResult addRole(String access_token,String username,Integer role){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("username",username);
        request.put("role",role);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/role/addrole")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,AddRoleResult.class);
    }

    public static BaseResult deleteRole(String access_token,String username,Integer role){
        HashMap<String, Object> request = new HashMap<String,Object>();
        request.put("username",username);
        request.put("role",role);
        String json = JsonUtil.toJSONString(request);
        HttpUriRequest httpUriRequest = RequestBuilder.post()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/role/deleterole")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .setEntity(new StringEntity(json, Charset.forName("utf-8")))
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,AddGoodsResult.class);
    }

    public static RoleListResult getRoleList(String access_token,Integer offset,Integer limit,String keyword,Integer role){
        HashMap<String, Object> request = new HashMap<String,Object>();
        HttpUriRequest httpUriRequest = RequestBuilder.get()
                .setHeader(jsonHeader)
                .setUri(BASE_URI + "/wxaapi/broadcast/role/getrolelist")
                .addParameter(PARAM_ACCESS_TOKEN, API.accessToken(access_token))
                .addParameter("role",role.toString())
                .addParameter("offset",offset.toString())
                .addParameter("limit",limit.toString())
                .addParameter("keyword",keyword)
                .build();
        return LocalHttpClient.executeJsonResult(httpUriRequest,RoleListResult.class);
    }
}